local lsp = require('lsp-zero').preset({})

lsp.on_attach(function(client, bufnr)
  lsp.default_keymaps({
      buffer = bufnr,
      preserve_mappings = false
  })
end)

-- (Optional) Configure lua language server for neovim
require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())

lsp.setup()

-- You need to setup `cmp` after lsp-zero
local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

require('luasnip.loaders.from_snipmate').lazy_load()

cmp.setup({
  completion = {
    autocomplete = false
  },
  snippet = {
      expand = function(args)
          require 'luasnip'.lsp_expand(args.body)
      end
  },
  sources = {
    {name = 'luasnip'},
    {name = 'nvim_lsp'},
    {name = 'path'},
  },
  mapping = {
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<Tab>'] = cmp_action.luasnip_supertab(),
    ['<S-Tab>'] = cmp_action.luasnip_shift_supertab(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
   }
})
