local session_manager = require('session_manager')
local config = require('session_manager.config')

vim.keymap.set('n', '<leader>pt', session_manager.load_session, {})
vim.keymap.set('n', '<leader>ps', session_manager.save_current_session, {})
vim.keymap.set('n', '<leader>pd', session_manager.delete_session, {})
vim.keymap.set('n', '<leader>pl', session_manager.load_last_session, {})

session_manager.setup({
    autoload_mode = config.AutoloadMode.Disabled,
    autosave_only_in_session = true
})
