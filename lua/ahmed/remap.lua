vim.g.mapleader = " "

-- Arabic input
vim.keymap.set("n", "<leader>a", function()
    vim.opt.keymap = "arabic-dvorak"
    vim.opt.rightleft = true
end)

vim.keymap.set("n", "<leader>e", function()
    vim.opt.keymap = ""
    vim.opt.rightleft = false
end)

vim.keymap.set("n", "<leader>oh", ":set invhlsearch<CR>")

vim.keymap.set("n", "<leader>m", vim.cmd.Ex)
vim.keymap.set("n", "<leader>w", "<C-w>")

vim.keymap.set("n", "<CR>", "i<CR><ESC>")
vim.keymap.set("n", "<ESC>", ":w<CR>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "<C-d>", "<C-d>zz")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("n", "<leader>s", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")

vim.keymap.set("n", "<C-j>", ":cnext<CR>")
vim.keymap.set("n", "<C-k>", ":cprev<CR>")

-- Open relative file
vim.keymap.set("n", "<leader>cr", ":e <C-R>=expand(\"%:.:h\") . \"/\"<CR>")
-- Change current working directory
vim.keymap.set("n", "<leader>ch", ":lcd %:h<CR>")

-- Netrw config
vim.cmd[[
" Hide dot files by default
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" highlight marked files like search matches
hi! link netrwMarkFile Search

augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup END

function! NetrwMapping()
    noremap <buffer> <leader>ch :lcd %<CR>
    nmap <buffer> <TAB> mfj
    nmap <buffer> <S-TAB> mfk
endfunction
]]
